//
//  AppDelegate.m
//  NSOutlineView-Simplest
//
//  Created by AlaudaProjects on 30.04.2012.
//  Copyright (c) 2012 AlaudaProjects. All rights reserved.
//  Re-cloned in MAvericks on May 30th, 2014
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;


- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    //initialize stuff
}

- (void)awakeFromNib
{
    dataSource = [NSArray arrayWithObjects:@"John", @"Mary", @"George", @"James", @"Niagara", @"Kathryn", nil];
    
#if DEBUG
    NSLog(@"%@", dataSource);
#endif
    
#if DEBUG
    NSLog(@"nib awaken");
#endif
}

- (void)dealloc
{
    [dataSource release];
    [super dealloc];
}

#pragma mark -
#pragma mark OutlineView Data Source Delegate Methods



// -------------------------------------------------------------------------------
//	– outlineView(OBJECT):child(NSInteger):ofItem:(id)
//  Returns the object that will be displayed in the tree
// -------------------------------------------------------------------------------
- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
    
    /*
     Here the outline view is asking you for a child of a particular item.
     
     If item is nil it is asking for a child of the root of the tree. You have only one top level item - the first element of the array, do if item is nil, you return [dataArray objectAtIndex: 0].
     
     If the item is the top level item i.e. if item == [dataArray objectAtIndex: 0], you need to return one of its children. Which one you return depends on index, naturally.
     */
    
    /*
     
     if (item == nil) {
     return [dataSource objectAtIndex:index];
     } else {
     return [dataSource objectAtIndex:0];
     }
     
     */
    
    return [dataSource objectAtIndex:index];
    //return item;
    
}// end – outlineView(OBJECT):child(NSInteger):ofItem:(id)

// -------------------------------------------------------------------------------
//	– outlineView(OBJECT):child(NSInteger):ofItem:(id)
// -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    //Here the outline view is asking you if an item is expandable. You need to return YES for the top level item, NO for the others.
    /*
     if (item == nil) {
     return YES;
     } else {
     return NO;
     }
     */
    //return YES;
    if (item == nil) {
        return YES;
    }
    
    return NO;
    
}// end



// -------------------------------------------------------------------------------
//	– outlineView(OBJECT):child(NSInteger):ofItem:(id)
// -------------------------------------------------------------------------------
- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    /*
     Here the outline view is asking for the number of children of a particular item. If the passed in item is nil, you should return 1 (the root has one child, your top level item). If the passed in item is your top level item, you should return 4 which is the number of items below it. In other cases you should return 0.
     */
    /*
     if (item == nil) {
     return [dataSource count];
     } else {
     return 0;
     }
     */
    
    return [dataSource count];
    
}// end



// -------------------------------------------------------------------------------
//	– outlineView(OBJECT):child(NSInteger):ofItem:(id)
// -------------------------------------------------------------------------------
- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
    /*
     Here the outline view is asking for a value to display in a particular column for a particular item. If you have only one column and your array contains strings, you can just return the passed in item.
     */
    /*
     NSString *result = nil;
     
     if (item == nil) {
     result = nil;
     } else {
     result = item;
     }
     
     return result;
     */
    //return @"boo";
    
    if ([[tableColumn identifier] isEqualToString:@"fColumn"]) {
        /*
         if (item == nil) {
         return @"My family";
         }
         */
        return item == nil ? @"My family" : item;
    }
    
    if ([[tableColumn identifier] isEqualToString:@"sColumn"]) {
        //return item;
        return nil;
    }
    
    
    return item;
    
}// end

@end