//
//  AppDelegate.h
//  NSOutlineView-Simplest
//
//  Created by AlaudaProjects on 30.04.2012.
//  Copyright (c) 2012 AlaudaProjects. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSOutlineViewDataSource> {
    IBOutlet NSOutlineView *myOutlineView;
    NSArray *dataSource;
}

@property (assign) IBOutlet NSWindow *window;


@end
