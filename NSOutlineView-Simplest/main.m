//
//  main.m
//  NSOutlineView-Simplest
//
//  Created by AlaudaProjects on 30.04.2012.
//  Copyright (c) 2012 AlaudaProjects. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
